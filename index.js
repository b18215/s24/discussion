//ES6 Updates


// Exponent Operators (**)
const firstNum = 8 ** 2;
console.log("Result as per ES6 updates");
console.log(firstNum);
//result: 64

	// before ES6
	const secondNum = Math.pow(8, 2)
	console.log("secondNum Result - Before ES6 updates");
	console.log(secondNum);
	//result: 64


// Exponent Operators (**)
const getCube = 5 ** 3;
console.log("Result as per ES6 updates");
console.log(getCube);
//result: 125

//Template Literals
let name = "Tine";

//before the update
let message = ('Hello ' + name + '! ' + 'Welcome to programming');
console.log('Result from pre-template literals')
console.log(message)

//ES6 Updates
message = (`Hello ${name}! Welcome to programming.`)
console.log('Result from with template literals')
console.log(message)



let string1 = "Zuitt";
let string2 = "Coding";
let string3 = "Bootcamp";
let string4 = "teaches";
let string5 = "Javascript";
let string6 = "as a";
let string7 = "programming language.";

console.log(` ${string1}  ${string2}  ${string3}  ${string4}  ${string5}  ${string6}  ${string7}`)

const interestRate = .1;
const principal = 1000;

console.log(`The interest in your savings account is: ${principal * interestRate}`);
// result: 100.00



// Array Destructing
/*
	Syntax:
		let / const [variableNameA, variableNameB, ...]
					arrayName
*/


const fullName = ["Juan", "Dela", "Cruz"];

// before update
console.log(fullName[0])
console.log(fullName[1])
console.log(fullName[2])

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

// ES6 Update

const [firstName, middleName, lastName] = fullName ;

console.log(firstName);
console.log(middleName);
console.log(lastName);


let pointGuards = ['Curry', 'Lillard', 'Paul', 'Irving'];
const [pointGuard1, pointGuard2, pointGuard3, pointGuard4] = pointGuards ;

console.log(pointGuard1);
console.log(pointGuard2);
console.log(pointGuard3);
console.log(pointGuard4);

// Before updates
const person = {
	givenName: 'Jane',
	middleName2: 'Dela',
	lastName2: 'Cruz',
};


// Object Destructure
// 



/*
	Syntax:
		let / const {propertyNameA, propertyNameB, ... } = objectName

*/




console.log(person.givenName);
console.log(person.middleName2);
console.log(person.lastName2);

console.log(`Hello ${person.givenName} ${person.middleName} ${person.lastName2} it was nice meeting you.`);

// ES6 Updates
const {givenName, middleName2, lastName2} = person;

console.log(givenName);
console.log(middleName2);
console.log(lastName2);

console.log(`Hello ${givenName} ${middleName2} ${lastName2} it was nice meeting you.`);


// My Pokemon is <pokemon1Name>, it is level <levelOfPokemon>, it is a <TypeofPokemon> type.


let pokemon1 = {
	pokename:"Charmander",
	level: 11,
	type: "Fire"
};

const {pokename, level, type} = pokemon1;

console.log(pokename);
console.log(level);
console.log(type);

console.log(`My Pokemon is ${pokename}, it is level ${level}, it is a ${type} type.`);


// Arrow Function
/*
	Syntax:
		const variableName = (parameter) => {
			statement / code block
		}
*/

// Before update


function printFullName(firstName, middleName, lastName){
	console.log(`${firstName} ${middleName} ${lastName}`)
};
printFullName('John', 'D', 'Smith');

// ES6 Update

const printFullName2 = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`)
}
printFullName2('John', 'D', 'Smith');

const students = ["John", "Jane", "Juan"];

/*students.forEach(function)()*/


// Arrow Function using forEach
students.forEach(student => {
    console.log(`${student} is a student.`)
});


/*// explicit return
const add = (x, y) => {
	return x + y
};

let total = add(9, 18);
console.log(total);
// result: 27*/

// With implicit return
const add = (x, y) => x + y
let total = add(9, 18);
console.log(total);
// result: 27


const greet = (name = 'User') => {
	return `Good day! ${name}.`
}
console.log(greet());


class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}


let myCar = new Car();
console.log(myCar);
// result: undefined

myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = 2021;

console.log(myCar);

const newCar = new Car("Toyota", "Vios", 2021);
console.log(newCar);
